/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rigxo
 */
public class tutor {
    private String tutor_rut;
    private String tutor_nombre;
    private String tutor_contacto;

    public tutor() {
    }

    public tutor(String tutor_rut, String tutor_nombre, String tutor_contacto) {
        this.tutor_rut = tutor_rut;
        this.tutor_nombre = tutor_nombre;
        this.tutor_contacto = tutor_contacto;
    }

    public String getTutor_rut() {
        return tutor_rut;
    }

    public void setTutor_rut(String tutor_rut) {
        this.tutor_rut = tutor_rut;
    }

    public String getTutor_nombre() {
        return tutor_nombre;
    }

    public void setTutor_nombre(String tutor_nombre) {
        this.tutor_nombre = tutor_nombre;
    }

    public String getTutor_contacto() {
        return tutor_contacto;
    }

    public void setTutor_contacto(String tutor_contacto) {
        this.tutor_contacto = tutor_contacto;
    }
    
    
}
