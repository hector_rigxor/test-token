/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class caracteristicasConductuales {
    private int cc_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean cc_tranquilo;
    private String cc_tranquilo_des;
    private boolean cc_inquieto;
    private String cc_inquieto_des;
    private boolean cc_pelea;
    private String cc_pelea_des;
    private boolean cc_no_respeta_normas;
    private String cc_no_respeta_normas_des;

    public caracteristicasConductuales() {
    }

    public caracteristicasConductuales(boolean cc_tranquilo, boolean cc_inquieto, boolean cc_pelea, boolean cc_no_respeta_normas) {
        this.cc_codigo = count.incrementAndGet();
        this.cc_tranquilo = cc_tranquilo;
        this.cc_inquieto = cc_inquieto;
        this.cc_pelea = cc_pelea;
        this.cc_no_respeta_normas = cc_no_respeta_normas;
    }

    public caracteristicasConductuales(boolean cc_tranquilo, String cc_tranquilo_des, boolean cc_inquieto, String cc_inquieto_des, boolean cc_pelea, String cc_pelea_des, boolean cc_no_respeta_normas, String cc_no_respeta_normas_des) {
        this.cc_codigo = count.incrementAndGet();
        this.cc_tranquilo = cc_tranquilo;
        this.cc_tranquilo_des = cc_tranquilo_des;
        this.cc_inquieto = cc_inquieto;
        this.cc_inquieto_des = cc_inquieto_des;
        this.cc_pelea = cc_pelea;
        this.cc_pelea_des = cc_pelea_des;
        this.cc_no_respeta_normas = cc_no_respeta_normas;
        this.cc_no_respeta_normas_des = cc_no_respeta_normas_des;
    }

    public int getCc_codigo() {
        return cc_codigo;
    }

    public void setCc_codigo(int cc_codigo) {
        this.cc_codigo = cc_codigo;
    }

    public boolean isCc_tranquilo() {
        return cc_tranquilo;
    }

    public void setCc_tranquilo(boolean cc_tranquilo) {
        this.cc_tranquilo = cc_tranquilo;
    }

    public String getCc_tranquilo_des() {
        return cc_tranquilo_des;
    }

    public void setCc_tranquilo_des(String cc_tranquilo_des) {
        this.cc_tranquilo_des = cc_tranquilo_des;
    }

    public boolean isCc_inquieto() {
        return cc_inquieto;
    }

    public void setCc_inquieto(boolean cc_inquieto) {
        this.cc_inquieto = cc_inquieto;
    }

    public String getCc_inquieto_des() {
        return cc_inquieto_des;
    }

    public void setCc_inquieto_des(String cc_inquieto_des) {
        this.cc_inquieto_des = cc_inquieto_des;
    }

    public boolean isCc_pelea() {
        return cc_pelea;
    }

    public void setCc_pelea(boolean cc_pelea) {
        this.cc_pelea = cc_pelea;
    }

    public String getCc_pelea_des() {
        return cc_pelea_des;
    }

    public void setCc_pelea_des(String cc_pelea_des) {
        this.cc_pelea_des = cc_pelea_des;
    }

    public boolean isCc_no_respeta_normas() {
        return cc_no_respeta_normas;
    }

    public void setCc_no_respeta_normas(boolean cc_no_respeta_normas) {
        this.cc_no_respeta_normas = cc_no_respeta_normas;
    }

    public String getCc_no_respeta_normas_des() {
        return cc_no_respeta_normas_des;
    }

    public void setCc_no_respeta_normas_des(String cc_no_respeta_normas_des) {
        this.cc_no_respeta_normas_des = cc_no_respeta_normas_des;
    }
    
    
}
