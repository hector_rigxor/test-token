/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class evaluadoAnteriormente {
    private int ea__codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean ea_neurologo;
    private boolean ea_psiquiata;
    private boolean ea_psicologo;
    private boolean ea_fonoaudiologo;
    private String ea_otro;
    private String ea_diagnostico;
    private String ea_observaciones;

    public evaluadoAnteriormente() {
    }

    
    
    public evaluadoAnteriormente(boolean ea_neurologo, boolean ea_psiquiata, boolean ea_psicologo, boolean ea_fonoaudiologo, String ea_otro, String ea_diagnostico, String ea_observaciones) {
        this.ea__codigo = count.incrementAndGet();
        this.ea_neurologo = ea_neurologo;
        this.ea_psiquiata = ea_psiquiata;
        this.ea_psicologo = ea_psicologo;
        this.ea_fonoaudiologo = ea_fonoaudiologo;
        this.ea_otro = ea_otro;
        this.ea_diagnostico = ea_diagnostico;
        this.ea_observaciones = ea_observaciones;
    }

    public int getEa__codigo() {
        return ea__codigo;
    }

    public void setEa__codigo(int ea__codigo) {
        this.ea__codigo = ea__codigo;
    }

    public boolean isEa_neurologo() {
        return ea_neurologo;
    }

    public void setEa_neurologo(boolean ea_neurologo) {
        this.ea_neurologo = ea_neurologo;
    }

    public boolean isEa_psiquiata() {
        return ea_psiquiata;
    }

    public void setEa_psiquiata(boolean ea_psiquiata) {
        this.ea_psiquiata = ea_psiquiata;
    }

    public boolean isEa_psicologo() {
        return ea_psicologo;
    }

    public void setEa_psicologo(boolean ea_psicologo) {
        this.ea_psicologo = ea_psicologo;
    }

    public boolean isEa_fonoaudiologo() {
        return ea_fonoaudiologo;
    }

    public void setEa_fonoaudiologo(boolean ea_fonoaudiologo) {
        this.ea_fonoaudiologo = ea_fonoaudiologo;
    }

    public String getEa_otro() {
        return ea_otro;
    }

    public void setEa_otro(String ea_otro) {
        this.ea_otro = ea_otro;
    }

    public String getEa_diagnostico() {
        return ea_diagnostico;
    }

    public void setEa_diagnostico(String ea_diagnostico) {
        this.ea_diagnostico = ea_diagnostico;
    }

    public String getEa_observaciones() {
        return ea_observaciones;
    }

    public void setEa_observaciones(String ea_observaciones) {
        this.ea_observaciones = ea_observaciones;
    }
    
    
}
