/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class antecedentesFamilia {
    private int af_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private String af_con_quien_vive;
    private boolean af_enfermedad_familiar;
    private String af_enfermedad_familiar_des;
    private String af_quien_cuida;
    private String af_quien_ayuda_tareas;
    private String af_inf_extra;
    private String af_persona_entrevistada;

    public antecedentesFamilia() {
    }

    public antecedentesFamilia(String af_con_quien_vive, boolean af_enfermedad_familiar, String af_quien_cuida, String af_quien_ayuda_tareas, String af_inf_extra, String af_persona_entrevistada) {
        this.af_codigo = count.incrementAndGet();
        this.af_con_quien_vive = af_con_quien_vive;
        this.af_enfermedad_familiar = af_enfermedad_familiar;
        this.af_quien_cuida = af_quien_cuida;
        this.af_quien_ayuda_tareas = af_quien_ayuda_tareas;
        this.af_inf_extra = af_inf_extra;
        this.af_persona_entrevistada = af_persona_entrevistada;
    }

    public int getAf_codigo() {
        return af_codigo;
    }

    public void setAf_codigo(int af_codigo) {
        this.af_codigo = af_codigo;
    }

    public String getAf_con_quien_vive() {
        return af_con_quien_vive;
    }

    public void setAf_con_quien_vive(String af_con_quien_vive) {
        this.af_con_quien_vive = af_con_quien_vive;
    }

    public boolean isAf_enfermedad_familiar() {
        return af_enfermedad_familiar;
    }

    public void setAf_enfermedad_familiar(boolean af_enfermedad_familiar) {
        this.af_enfermedad_familiar = af_enfermedad_familiar;
    }

    public String getAf_enfermedad_familiar_des() {
        return af_enfermedad_familiar_des;
    }

    public void setAf_enfermedad_familiar_des(String af_enfermedad_familiar_des) {
        this.af_enfermedad_familiar_des = af_enfermedad_familiar_des;
    }

    public String getAf_quien_cuida() {
        return af_quien_cuida;
    }

    public void setAf_quien_cuida(String af_quien_cuida) {
        this.af_quien_cuida = af_quien_cuida;
    }

    public String getAf_quien_ayuda_tareas() {
        return af_quien_ayuda_tareas;
    }

    public void setAf_quien_ayuda_tareas(String af_quien_ayuda_tareas) {
        this.af_quien_ayuda_tareas = af_quien_ayuda_tareas;
    }

    public String getAf_inf_extra() {
        return af_inf_extra;
    }

    public void setAf_inf_extra(String af_inf_extra) {
        this.af_inf_extra = af_inf_extra;
    }

    public String getAf_persona_entrevistada() {
        return af_persona_entrevistada;
    }

    public void setAf_persona_entrevistada(String af_persona_entrevistada) {
        this.af_persona_entrevistada = af_persona_entrevistada;
    }
    
    
}
