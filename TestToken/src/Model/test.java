/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class test {
    private int test_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private int test_correctas;
    private int test_incorrectas;
    private int test_puntaje;

    public test() {
    }

    public test(int test_correctas, int test_incorrectas, int test_puntaje) {
        this.test_codigo = count.incrementAndGet();
        this.test_correctas = test_correctas;
        this.test_incorrectas = test_incorrectas;
        this.test_puntaje = test_puntaje;
    }

    public int getTest_codigo() {
        return test_codigo;
    }

    public void setTest_codigo(int test_codigo) {
        this.test_codigo = test_codigo;
    }

    public int getTest_correctas() {
        return test_correctas;
    }

    public void setTest_correctas(int test_correctas) {
        this.test_correctas = test_correctas;
    }

    public int getTest_incorrectas() {
        return test_incorrectas;
    }

    public void setTest_incorrectas(int test_incorrectas) {
        this.test_incorrectas = test_incorrectas;
    }

    public int getTest_puntaje() {
        return test_puntaje;
    }

    public void setTest_puntaje(int test_puntaje) {
        this.test_puntaje = test_puntaje;
    }
    
    
}
