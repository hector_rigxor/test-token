/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class anamnesis {
    private int an_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private LocalDate an_fecha;
    private String psp_nombre;
    private alumno al_codigo;

    public anamnesis(LocalDate an_fecha, String psp_nombre, alumno al_codigo) {
        this.an_codigo = count.incrementAndGet();
        this.an_fecha = an_fecha;
        this.psp_nombre = psp_nombre;
        this.al_codigo = al_codigo;
    }

    public anamnesis() {
    }

    public int getAn_codigo() {
        return an_codigo;
    }

    public void setAn_codigo(int an_codigo) {
        this.an_codigo = an_codigo;
    }

    public LocalDate getAn_fecha() {
        return an_fecha;
    }

    public void setAn_fecha(LocalDate an_fecha) {
        this.an_fecha = an_fecha;
    }

    public String getPsp_nombre() {
        return psp_nombre;
    }

    public void setPsp_nombre(String psp_nombre) {
        this.psp_nombre = psp_nombre;
    }

    public alumno getAl_codigo() {
        return al_codigo;
    }

    public void setAl_codigo(alumno al_codigo) {
        this.al_codigo = al_codigo;
    }
    
    
}
