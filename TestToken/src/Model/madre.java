/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rigxo
 */
public class madre {
    private String madre_rut;
    private String madre_nombre;
    private String madre_contacto;
    private int madre_edad;
    private String madre_ocupacion;
    private String madre_escolaridad;

    public madre() {
    }

    public madre(String madre_rut, String madre_nombre, String madre_contacto) {
        this.madre_rut = madre_rut;
        this.madre_nombre = madre_nombre;
        this.madre_contacto = madre_contacto;
    }

    public String getMadre_rut() {
        return madre_rut;
    }

    public void setMadre_rut(String madre_rut) {
        this.madre_rut = madre_rut;
    }

    public String getMadre_nombre() {
        return madre_nombre;
    }

    public void setMadre_nombre(String madre_nombre) {
        this.madre_nombre = madre_nombre;
    }

    public String getMadre_contacto() {
        return madre_contacto;
    }

    public void setMadre_contacto(String madre_contacto) {
        this.madre_contacto = madre_contacto;
    }

    public int getMadre_edad() {
        return madre_edad;
    }

    public void setMadre_edad(int madre_edad) {
        this.madre_edad = madre_edad;
    }

    public String getMadre_ocupacion() {
        return madre_ocupacion;
    }

    public void setMadre_ocupacion(String madre_ocupacion) {
        this.madre_ocupacion = madre_ocupacion;
    }

    public String getMadre_escolaridad() {
        return madre_escolaridad;
    }

    public void setMadre_escolaridad(String madre_escolaridad) {
        this.madre_escolaridad = madre_escolaridad;
    }
    
}
