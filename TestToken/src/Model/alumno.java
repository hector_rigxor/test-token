/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;

/**
 *
 * @author Rigxo
 */
public class alumno {
    private String al_rut;
    private String al_nombre_completo;
    private LocalDate al_fecha_nac;
    private int al_edad;
    private String al_curso;
    private String al_direccion;
    private padre padre_rut;
    private madre madre_rut;
    private tutor tutor_rut;
    private desarrollo des_cod;
    private antecedentesEscuela antEsc_cod;
    private antecedentesFamilia antFam_cod;
    private test test_cod;

    public alumno(String al_rut, String al_nombre_completo, LocalDate al_fecha_nac, int al_edad, String al_curso, String al_direccion) {
        this.al_rut = al_rut;
        this.al_nombre_completo = al_nombre_completo;
        this.al_fecha_nac = al_fecha_nac;
        this.al_edad = al_edad;
        this.al_curso = al_curso;
        this.al_direccion = al_direccion;
    }

    public alumno() {
    }

    public alumno(String al_rut, String al_nombre_completo, LocalDate al_fecha_nac, int al_edad, String al_curso, String al_direccion, padre padre_rut, madre madre_rut, tutor tutor_rut, desarrollo des_cod, antecedentesEscuela antEsc_cod, antecedentesFamilia antFam_cod, test test_cod) {
        this.al_rut = al_rut;
        this.al_nombre_completo = al_nombre_completo;
        this.al_fecha_nac = al_fecha_nac;
        this.al_edad = al_edad;
        this.al_curso = al_curso;
        this.al_direccion = al_direccion;
        this.padre_rut = padre_rut;
        this.madre_rut = madre_rut;
        this.tutor_rut = tutor_rut;
        this.des_cod = des_cod;
        this.antEsc_cod = antEsc_cod;
        this.antFam_cod = antFam_cod;
        this.test_cod = test_cod;
    }
 
    public String getAl_rut() {
        return al_rut;
    }

    public void setAl_rut(String al_rut) {
        this.al_rut = al_rut;
    }

    public String getAl_nombre_completo() {
        return al_nombre_completo;
    }

    public void setAl_nombre_completo(String al_nombre_completo) {
        this.al_nombre_completo = al_nombre_completo;
    }

    public LocalDate getAl_fecha_nac() {
        return al_fecha_nac;
    }

    public void setAl_fecha_nac(LocalDate al_fecha_nac) {
        this.al_fecha_nac = al_fecha_nac;
    }

    public int getAl_edad() {
        return al_edad;
    }

    public void setAl_edad(int al_edad) {
        this.al_edad = al_edad;
    }

    public String getAl_curso() {
        return al_curso;
    }

    public void setAl_curso(String al_curso) {
        this.al_curso = al_curso;
    }

    public String getAl_direccion() {
        return al_direccion;
    }

    public void setAl_direccion(String al_direccion) {
        this.al_direccion = al_direccion;
    }

    public padre getPadre_rut() {
        return padre_rut;
    }

    public void setPadre_rut(padre padre_rut) {
        this.padre_rut = padre_rut;
    }

    public madre getMadre_rut() {
        return madre_rut;
    }

    public void setMadre_rut(madre madre_rut) {
        this.madre_rut = madre_rut;
    }

    public tutor getTutor_rut() {
        return tutor_rut;
    }

    public void setTutor_rut(tutor tutor_rut) {
        this.tutor_rut = tutor_rut;
    }

    public desarrollo getDes_cod() {
        return des_cod;
    }

    public void setDes_cod(desarrollo des_cod) {
        this.des_cod = des_cod;
    }

    public antecedentesEscuela getAntEsc_cod() {
        return antEsc_cod;
    }

    public void setAntEsc_cod(antecedentesEscuela antEsc_cod) {
        this.antEsc_cod = antEsc_cod;
    }

    public antecedentesFamilia getAntFam_cod() {
        return antFam_cod;
    }

    public void setAntFam_cod(antecedentesFamilia antFam_cod) {
        this.antFam_cod = antFam_cod;
    }

    public test getTest_cod() {
        return test_cod;
    }

    public void setTest_cod(test test_cod) {
        this.test_cod = test_cod;
    }

    @Override
    public String toString() {
        return "alumno{" + "nombre= " + al_nombre_completo + ", edad= " + al_edad + '}';
    }
    
    
}
