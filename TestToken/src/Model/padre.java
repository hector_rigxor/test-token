/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Rigxo
 */
public class padre {
    private String padre_rut;
    private String padre_nombre;
    private String padre_contacto;
    private int padre_edad;
    private String padre_ocupacion;
    private String padre_escolaridad;

    public padre() {
    }

    public padre(String padre_rut, String padre_nombre, String padre_contacto) {
        this.padre_rut = padre_rut;
        this.padre_nombre = padre_nombre;
        this.padre_contacto = padre_contacto;
    }

    public String getPadre_rut() {
        return padre_rut;
    }

    public void setPadre_rut(String padre_rut) {
        this.padre_rut = padre_rut;
    }

    public String getPadre_nombre() {
        return padre_nombre;
    }

    public void setPadre_nombre(String padre_nombre) {
        this.padre_nombre = padre_nombre;
    }

    public String getPadre_contacto() {
        return padre_contacto;
    }

    public void setPadre_contacto(String padre_contacto) {
        this.padre_contacto = padre_contacto;
    }

    public int getPadre_edad() {
        return padre_edad;
    }

    public void setPadre_edad(int padre_edad) {
        this.padre_edad = padre_edad;
    }

    public String getPadre_ocupacion() {
        return padre_ocupacion;
    }

    public void setPadre_ocupacion(String padre_ocupacion) {
        this.padre_ocupacion = padre_ocupacion;
    }

    public String getPadre_escolaridad() {
        return padre_escolaridad;
    }

    public void setPadre_escolaridad(String padre_escolaridad) {
        this.padre_escolaridad = padre_escolaridad;
    }
    
    
}
