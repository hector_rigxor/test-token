/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class desarrollo {
    private int des_cod;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean des_enfermedades_importantes;
    private String des_cual_enf_imp;
    private boolean des_golpes_tec;
    private boolean des_sintomas_epilepsia;
    private boolean des_enf_vista;
    private boolean des_enf_oido;
    private lenguaje lenguaje_cod;
    private sueño sueño_cod;
    private evaluadoAnteriormente evaAnt_cod;
    private desarrolloMotor desMot_cod;
    private caracteristicasConductuales carCon_cod;
    private tratadoActualmente traAct_cod;

    public desarrollo() {
    }

    
    
    public desarrollo(boolean des_enfermedades_importantes, String des_cual_enf_imp, boolean des_golpes_tec, boolean des_sintomas_epilepsia, boolean des_enf_vista, boolean des_enf_oido) {
        this.des_cod = count.incrementAndGet();
        this.des_enfermedades_importantes = des_enfermedades_importantes;
        this.des_cual_enf_imp = des_cual_enf_imp;
        this.des_golpes_tec = des_golpes_tec;
        this.des_sintomas_epilepsia = des_sintomas_epilepsia;
        this.des_enf_vista = des_enf_vista;
        this.des_enf_oido = des_enf_oido;
    }

    public int getDes_cod() {
        return des_cod;
    }

    public void setDes_cod(int des_cod) {
        this.des_cod = des_cod;
    }

    public boolean isDes_enfermedades_importantes() {
        return des_enfermedades_importantes;
    }

    public void setDes_enfermedades_importantes(boolean des_enfermedades_importantes) {
        this.des_enfermedades_importantes = des_enfermedades_importantes;
    }

    public String getDes_cual_enf_imp() {
        return des_cual_enf_imp;
    }

    public void setDes_cual_enf_imp(String des_cual_enf_imp) {
        this.des_cual_enf_imp = des_cual_enf_imp;
    }

    public boolean isDes_golpes_tec() {
        return des_golpes_tec;
    }

    public void setDes_golpes_tec(boolean des_golpes_tec) {
        this.des_golpes_tec = des_golpes_tec;
    }

    public boolean isDes_sintomas_epilepsia() {
        return des_sintomas_epilepsia;
    }

    public void setDes_sintomas_epilepsia(boolean des_sintomas_epilepsia) {
        this.des_sintomas_epilepsia = des_sintomas_epilepsia;
    }

    public boolean isDes_enf_vista() {
        return des_enf_vista;
    }

    public void setDes_enf_vista(boolean des_enf_vista) {
        this.des_enf_vista = des_enf_vista;
    }

    public boolean isDes_enf_oido() {
        return des_enf_oido;
    }

    public void setDes_enf_oido(boolean des_enf_oido) {
        this.des_enf_oido = des_enf_oido;
    }

    public lenguaje getLenguaje_cod() {
        return lenguaje_cod;
    }

    public void setLenguaje_cod(lenguaje lenguaje_cod) {
        this.lenguaje_cod = lenguaje_cod;
    }

    public sueño getSueño_cod() {
        return sueño_cod;
    }

    public void setSueño_cod(sueño sueño_cod) {
        this.sueño_cod = sueño_cod;
    }

    public evaluadoAnteriormente getEvaAnt_cod() {
        return evaAnt_cod;
    }

    public void setEvaAnt_cod(evaluadoAnteriormente evaAnt_cod) {
        this.evaAnt_cod = evaAnt_cod;
    }

    public desarrolloMotor getDesMot_cod() {
        return desMot_cod;
    }

    public void setDesMot_cod(desarrolloMotor desMot_cod) {
        this.desMot_cod = desMot_cod;
    }

    public caracteristicasConductuales getCarCon_cod() {
        return carCon_cod;
    }

    public void setCarCon_cod(caracteristicasConductuales carCon_cod) {
        this.carCon_cod = carCon_cod;
    }

    public tratadoActualmente getTraAct_cod() {
        return traAct_cod;
    }

    public void setTraAct_cod(tratadoActualmente traAct_cod) {
        this.traAct_cod = traAct_cod;
    }
    
    
}
