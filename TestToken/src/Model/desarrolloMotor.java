/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class desarrolloMotor {
    private int dm_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private int dm_edad_caminar;
    private boolean dm_se_cae;
    private boolean dm_es_torpe;
    private boolean dm_cuesta_escribir_dibujar;
    private String dm_diagnostico_de;
    private String dm_apoyo_de;
    private String dm_lugaryespecialista;

    public desarrolloMotor() {
    }

    public desarrolloMotor(int dm_edad_caminar, boolean dm_se_cae, boolean dm_es_torpe, boolean dm_cuesta_escribir_dibujar, String dm_diagnostico_de, String dm_apoyo_de, String dm_lugaryespecialista) {
        this.dm_codigo = count.incrementAndGet();
        this.dm_edad_caminar = dm_edad_caminar;
        this.dm_se_cae = dm_se_cae;
        this.dm_es_torpe = dm_es_torpe;
        this.dm_cuesta_escribir_dibujar = dm_cuesta_escribir_dibujar;
        this.dm_diagnostico_de = dm_diagnostico_de;
        this.dm_apoyo_de = dm_apoyo_de;
        this.dm_lugaryespecialista = dm_lugaryespecialista;
    }

    public int getDm_codigo() {
        return dm_codigo;
    }

    public void setDm_codigo(int dm_codigo) {
        this.dm_codigo = dm_codigo;
    }

    public int getDm_edad_caminar() {
        return dm_edad_caminar;
    }

    public void setDm_edad_caminar(int dm_edad_caminar) {
        this.dm_edad_caminar = dm_edad_caminar;
    }

    public boolean isDm_se_cae() {
        return dm_se_cae;
    }

    public void setDm_se_cae(boolean dm_se_cae) {
        this.dm_se_cae = dm_se_cae;
    }

    public boolean isDm_es_torpe() {
        return dm_es_torpe;
    }

    public void setDm_es_torpe(boolean dm_es_torpe) {
        this.dm_es_torpe = dm_es_torpe;
    }

    public boolean isDm_cuesta_escribir_dibujar() {
        return dm_cuesta_escribir_dibujar;
    }

    public void setDm_cuesta_escribir_dibujar(boolean dm_cuesta_escribir_dibujar) {
        this.dm_cuesta_escribir_dibujar = dm_cuesta_escribir_dibujar;
    }

    public String getDm_diagnostico_de() {
        return dm_diagnostico_de;
    }

    public void setDm_diagnostico_de(String dm_diagnostico_de) {
        this.dm_diagnostico_de = dm_diagnostico_de;
    }

    public String getDm_apoyo_de() {
        return dm_apoyo_de;
    }

    public void setDm_apoyo_de(String dm_apoyo_de) {
        this.dm_apoyo_de = dm_apoyo_de;
    }

    public String getDm_lugaryespecialista() {
        return dm_lugaryespecialista;
    }

    public void setDm_lugaryespecialista(String dm_lugaryespecialista) {
        this.dm_lugaryespecialista = dm_lugaryespecialista;
    }
    
    
}
