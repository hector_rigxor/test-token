/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class lenguaje {
    private int len_cod;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean len_demora;
    private String len_demora_des;
    private boolean len_articular;
    private String len_articular_des;
    private boolean len_simple;
    private String len_simple_des;
    private boolean len_nohaceloquelepiden;
    private String len_nohaceloquelepiden_des;
    private boolean len_escuela;
    private String len_escuela_cual;
    private String len_recibe_apoyo_de;
    private String len_lugaryespecialista;

    public lenguaje() {
    }

    public lenguaje(boolean len_demora, boolean len_articular, boolean len_simple, boolean len_nohaceloquelepiden, boolean len_escuela, String len_recibe_apoyo_de, String len_lugaryespecialista) {
        this.len_cod = count.incrementAndGet();
        this.len_demora = len_demora;
        this.len_articular = len_articular;
        this.len_simple = len_simple;
        this.len_nohaceloquelepiden = len_nohaceloquelepiden;
        this.len_escuela = len_escuela;
        this.len_recibe_apoyo_de = len_recibe_apoyo_de;
        this.len_lugaryespecialista = len_lugaryespecialista;
    }

    public int getLen_cod() {
        return len_cod;
    }

    public void setLen_cod(int len_cod) {
        this.len_cod = len_cod;
    }

    public boolean isLen_demora() {
        return len_demora;
    }

    public void setLen_demora(boolean len_demora) {
        this.len_demora = len_demora;
    }

    public String getLen_demora_des() {
        return len_demora_des;
    }

    public void setLen_demora_des(String len_demora_des) {
        this.len_demora_des = len_demora_des;
    }

    public boolean isLen_articular() {
        return len_articular;
    }

    public void setLen_articular(boolean len_articular) {
        this.len_articular = len_articular;
    }

    public String getLen_articular_des() {
        return len_articular_des;
    }

    public void setLen_articular_des(String len_articular_des) {
        this.len_articular_des = len_articular_des;
    }

    public boolean isLen_simple() {
        return len_simple;
    }

    public void setLen_simple(boolean len_simple) {
        this.len_simple = len_simple;
    }

    public String getLen_simple_des() {
        return len_simple_des;
    }

    public void setLen_simple_des(String len_simple_des) {
        this.len_simple_des = len_simple_des;
    }

    public boolean isLen_nohaceloquelepiden() {
        return len_nohaceloquelepiden;
    }

    public void setLen_nohaceloquelepiden(boolean len_nohaceloquelepiden) {
        this.len_nohaceloquelepiden = len_nohaceloquelepiden;
    }

    public String getLen_nohaceloquelepiden_des() {
        return len_nohaceloquelepiden_des;
    }

    public void setLen_nohaceloquelepiden_des(String len_nohaceloquelepiden_des) {
        this.len_nohaceloquelepiden_des = len_nohaceloquelepiden_des;
    }

    public boolean isLen_escuela() {
        return len_escuela;
    }

    public void setLen_escuela(boolean len_escuela) {
        this.len_escuela = len_escuela;
    }

    public String getLen_escuela_cual() {
        return len_escuela_cual;
    }

    public void setLen_escuela_cual(String len_escuela_cual) {
        this.len_escuela_cual = len_escuela_cual;
    }

    public String getLen_recibe_apoyo_de() {
        return len_recibe_apoyo_de;
    }

    public void setLen_recibe_apoyo_de(String len_recibe_apoyo_de) {
        this.len_recibe_apoyo_de = len_recibe_apoyo_de;
    }

    public String getLen_lugaryespecialista() {
        return len_lugaryespecialista;
    }

    public void setLen_lugaryespecialista(String len_lugaryespecialista) {
        this.len_lugaryespecialista = len_lugaryespecialista;
    }
    
    
}
