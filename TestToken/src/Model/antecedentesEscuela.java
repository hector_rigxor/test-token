/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class antecedentesEscuela {
    private int ae_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean ae_jardin;
    private String ae_jardin_nombre;
    private String ae_kinder_prekinder;
    private boolean ae_repetido;
    private String ae_repetido_curso;
    private String ae_repetido_razon;
    private String ae_asignaturas_dificiles;

    public antecedentesEscuela() {
    }

    
    
    public antecedentesEscuela(boolean ae_jardin, String ae_jardin_nombre, String ae_kinder_prekinder, boolean ae_repetido, String ae_repetido_curso, String ae_repetido_razon, String ae_asignaturas_dificiles) {
        this.ae_codigo = count.incrementAndGet();
        this.ae_jardin = ae_jardin;
        this.ae_jardin_nombre = ae_jardin_nombre;
        this.ae_kinder_prekinder = ae_kinder_prekinder;
        this.ae_repetido = ae_repetido;
        this.ae_repetido_curso = ae_repetido_curso;
        this.ae_repetido_razon = ae_repetido_razon;
        this.ae_asignaturas_dificiles = ae_asignaturas_dificiles;
    }

    public antecedentesEscuela(boolean ae_jardin, boolean ae_repetido, String ae_asignaturas_dificiles) {
        this.ae_codigo = count.incrementAndGet();
        this.ae_jardin = ae_jardin;
        this.ae_repetido = ae_repetido;
        this.ae_asignaturas_dificiles = ae_asignaturas_dificiles;
    }
    

    public int getAe_codigo() {
        return ae_codigo;
    }

    public void setAe_codigo(int ae_codigo) {
        this.ae_codigo = ae_codigo;
    }

    public boolean isAe_jardin() {
        return ae_jardin;
    }

    public void setAe_jardin(boolean ae_jardin) {
        this.ae_jardin = ae_jardin;
    }

    public String getAe_jardin_nombre() {
        return ae_jardin_nombre;
    }

    public void setAe_jardin_nombre(String ae_jardin_nombre) {
        this.ae_jardin_nombre = ae_jardin_nombre;
    }

    public String getAe_kinder_prekinder() {
        return ae_kinder_prekinder;
    }

    public void setAe_kinder_prekinder(String ae_kinder_prekinder) {
        this.ae_kinder_prekinder = ae_kinder_prekinder;
    }

    public boolean isAe_repetido() {
        return ae_repetido;
    }

    public void setAe_repetido(boolean ae_repetido) {
        this.ae_repetido = ae_repetido;
    }

    public String getAe_repetido_curso() {
        return ae_repetido_curso;
    }

    public void setAe_repetido_curso(String ae_repetido_curso) {
        this.ae_repetido_curso = ae_repetido_curso;
    }

    public String getAe_repetido_razon() {
        return ae_repetido_razon;
    }

    public void setAe_repetido_razon(String ae_repetido_razon) {
        this.ae_repetido_razon = ae_repetido_razon;
    }

    public String getAe_asignaturas_dificiles() {
        return ae_asignaturas_dificiles;
    }

    public void setAe_asignaturas_dificiles(String ae_asignaturas_dificiles) {
        this.ae_asignaturas_dificiles = ae_asignaturas_dificiles;
    }
    
    
}
