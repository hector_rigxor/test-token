/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class tratadoActualmente {
    private int ta_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean ta_neurologo;
    private boolean ta_psiquiatra;
    private boolean ta_psicologo;
    private boolean ta_fonoaudiologo;
    private String ta_otro;
    private String ta_diagnostico;
    private String ta_tratamiento;
    private LocalDate ta_fecha_tratamiento;

    public tratadoActualmente() {
    }

    public tratadoActualmente(boolean ta_neurologo, boolean ta_psiquiatra, boolean ta_psicologo, boolean ta_fonoaudiologo, String ta_otro, String ta_diagnostico, String ta_tratamiento, LocalDate ta_fecha_tratamiento) {
        this.ta_codigo = count.incrementAndGet();
        this.ta_neurologo = ta_neurologo;
        this.ta_psiquiatra = ta_psiquiatra;
        this.ta_psicologo = ta_psicologo;
        this.ta_fonoaudiologo = ta_fonoaudiologo;
        this.ta_otro = ta_otro;
        this.ta_diagnostico = ta_diagnostico;
        this.ta_tratamiento = ta_tratamiento;
        this.ta_fecha_tratamiento = ta_fecha_tratamiento;
    }

    public int getTa_codigo() {
        return ta_codigo;
    }

    public void setTa_codigo(int ta_codigo) {
        this.ta_codigo = ta_codigo;
    }

    public boolean isTa_neurologo() {
        return ta_neurologo;
    }

    public void setTa_neurologo(boolean ta_neurologo) {
        this.ta_neurologo = ta_neurologo;
    }

    public boolean isTa_psiquiatra() {
        return ta_psiquiatra;
    }

    public void setTa_psiquiatra(boolean ta_psiquiatra) {
        this.ta_psiquiatra = ta_psiquiatra;
    }

    public boolean isTa_psicologo() {
        return ta_psicologo;
    }

    public void setTa_psicologo(boolean ta_psicologo) {
        this.ta_psicologo = ta_psicologo;
    }

    public boolean isTa_fonoaudiologo() {
        return ta_fonoaudiologo;
    }

    public void setTa_fonoaudiologo(boolean ta_fonoaudiologo) {
        this.ta_fonoaudiologo = ta_fonoaudiologo;
    }

    public String getTa_otro() {
        return ta_otro;
    }

    public void setTa_otro(String ta_otro) {
        this.ta_otro = ta_otro;
    }

    public String getTa_diagnostico() {
        return ta_diagnostico;
    }

    public void setTa_diagnostico(String ta_diagnostico) {
        this.ta_diagnostico = ta_diagnostico;
    }

    public String getTa_tratamiento() {
        return ta_tratamiento;
    }

    public void setTa_tratamiento(String ta_tratamiento) {
        this.ta_tratamiento = ta_tratamiento;
    }

    public LocalDate getTa_fecha_tratamiento() {
        return ta_fecha_tratamiento;
    }

    public void setTa_fecha_tratamiento(LocalDate ta_fecha_tratamiento) {
        this.ta_fecha_tratamiento = ta_fecha_tratamiento;
    }
    
    
}
