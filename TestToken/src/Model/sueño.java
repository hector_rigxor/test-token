/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Rigxo
 */
public class sueño {
    private int sp_codigo;
    private static final AtomicInteger count = new AtomicInteger(0);
    private boolean sp_inquieto;
    private String sp_inquieto_des;
    private boolean sp_pesadillas;
    private String sp_pesadillas_des;
    private boolean sp_dificultad;
    private String sp_dificultad_des;
    private boolean sp_sonambulo;
    private String sp_sonambulo_des;

    public sueño() {
    }

    
    
    public sueño(boolean sp_inquieto, boolean sp_pesadillas, boolean sp_dificultad, boolean sp_sonambulo) {
        this.sp_codigo = count.incrementAndGet();
        this.sp_inquieto = sp_inquieto;
        this.sp_pesadillas = sp_pesadillas;
        this.sp_dificultad = sp_dificultad;
        this.sp_sonambulo = sp_sonambulo;
    }

    public int getSp_codigo() {
        return sp_codigo;
    }

    public void setSp_codigo(int sp_codigo) {
        this.sp_codigo = sp_codigo;
    }

    public boolean isSp_inquieto() {
        return sp_inquieto;
    }

    public void setSp_inquieto(boolean sp_inquieto) {
        this.sp_inquieto = sp_inquieto;
    }

    public String getSp_inquieto_des() {
        return sp_inquieto_des;
    }

    public void setSp_inquieto_des(String sp_inquieto_des) {
        this.sp_inquieto_des = sp_inquieto_des;
    }

    public boolean isSp_pesadillas() {
        return sp_pesadillas;
    }

    public void setSp_pesadillas(boolean sp_pesadillas) {
        this.sp_pesadillas = sp_pesadillas;
    }

    public String getSp_pesadillas_des() {
        return sp_pesadillas_des;
    }

    public void setSp_pesadillas_des(String sp_pesadillas_des) {
        this.sp_pesadillas_des = sp_pesadillas_des;
    }

    public boolean isSp_dificultad() {
        return sp_dificultad;
    }

    public void setSp_dificultad(boolean sp_dificultad) {
        this.sp_dificultad = sp_dificultad;
    }

    public String getSp_dificultad_des() {
        return sp_dificultad_des;
    }

    public void setSp_dificultad_des(String sp_dificultad_des) {
        this.sp_dificultad_des = sp_dificultad_des;
    }

    public boolean isSp_sonambulo() {
        return sp_sonambulo;
    }

    public void setSp_sonambulo(boolean sp_sonambulo) {
        this.sp_sonambulo = sp_sonambulo;
    }

    public String getSp_sonambulo_des() {
        return sp_sonambulo_des;
    }

    public void setSp_sonambulo_des(String sp_sonambulo_des) {
        this.sp_sonambulo_des = sp_sonambulo_des;
    }
    
    
}
