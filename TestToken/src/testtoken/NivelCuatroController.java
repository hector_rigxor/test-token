/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class NivelCuatroController implements Initializable {

    @FXML
    private Button btn;
    
    @FXML
    private Button levelUp;
    
    private int puntos;
    
    private boolean AntOk;
    
    private int instruccion;
    @FXML
    public Label temp;
    
    private int primer_puntaje;
    
    private int segundo_puntaje;
    
    private int tercer_puntaje;
    
    private int cuarto_puntaje;
    
    
    /**
     * Initializes the controller class.
     */
    
    /**
     * This method accepts a person to initialize the view
     * @param puntaje
     * @param puntUno
     * @param puntDos
     * @param puntTres
     */
    public void initData(String puntaje, int puntUno, int puntDos, int puntTres)
    {
        temp.setText(puntaje);
        primer_puntaje = puntUno;
        segundo_puntaje = puntDos;
        tercer_puntaje = puntTres;
        System.out.println("Nivel uno: "+primer_puntaje);
        System.out.println("Nivel dos: "+segundo_puntaje);
        System.out.println("Nivel tres: "+tercer_puntaje);
        puntos = Integer.valueOf(temp.getText());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        instruccion = 0;
        levelUp.setVisible(false);
        cuarto_puntaje = 0;
    }
    
    @FXML
    private void mousePressed(MouseEvent event) throws InterruptedException {
    btn = (Button) event.getSource();
    //System.out.println("Pressed: " + btn.getId());

    instruccion = instruccion + 1;
    btn.setOnMouseEntered(new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {
        //System.out.println("true");
        }
        });
       
        switch(instruccion) {
  case 1:
      
    if (btn.getId().equals("cpAmarillo") && instruccion == 1){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 2:
      
    if (btn.getId().equals("sgVerde") && instruccion == 2 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 3:
    
    AntOk = false;
    if (btn.getId().equals("spAzul") && instruccion == 3){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 4:
      
    if (btn.getId().equals("cpVerde") && instruccion == 4 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 5:
    
    AntOk = false;
    if (btn.getId().equals("sgBlanco") && instruccion == 5){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 6:
      
    if (btn.getId().equals("cgRojo") && instruccion == 6 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 7:
    
    AntOk = false;
    if (btn.getId().equals("sgAzul") && instruccion == 7){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 8:
      
    if (btn.getId().equals("sgRojo") && instruccion == 8 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
  
  case 9:
    
    AntOk = false;
    if (btn.getId().equals("spAzul") && instruccion == 9){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 10:
      
    if (btn.getId().equals("cpAmarillo") && instruccion == 10 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 11:
    
    AntOk = false;
    if (btn.getId().equals("cpAzul") && instruccion == 11){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 12:
      
    if (btn.getId().equals("cpRojo") && instruccion == 12 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 13:
    
    AntOk = false;
    if (btn.getId().equals("sgAzul") && instruccion == 13){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 14:
      
    if (btn.getId().equals("sgVerde") && instruccion == 14 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 15:
    
    AntOk = false;
    if (btn.getId().equals("cgAzul") && instruccion == 15){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 16:
      
    if (btn.getId().equals("sgVerde") && instruccion == 16 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
  case 17:
    
    AntOk = false;
    if (btn.getId().equals("spRojo") && instruccion == 17){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 18:
      
    if (btn.getId().equals("cpAmarillo") && instruccion == 18 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        break;
    }
    
    case 19:
    
    AntOk = false;
    if (btn.getId().equals("spBlanco") && instruccion == 19){
        //Aspira a puntaje
        AntOk = true;
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        //No obtiene puntaje
        break;
    }
    
  case 20:
      
    if (btn.getId().equals("sgRojo") && instruccion == 20 && AntOk == true){
        //Obtiene puntaje
        puntos = puntos + 1;
        cuarto_puntaje = cuarto_puntaje + 1;
        levelUp.setVisible(true);
        temp.setText(String.valueOf(puntos));
        break;
    }
    else{
        System.out.println("Error "+instruccion);
        levelUp.setVisible(true);
        temp.setText(String.valueOf(puntos));
        System.out.println(temp.getText() + " Aqui");
        break;
    }
    
  default:
    System.out.println("Stage over");
}
        System.out.println("Puntos = "+puntos);
    }
    
    
    @FXML
    private void mouseReleased(MouseEvent event) {
    btn = (Button) event.getSource();
    //System.out.println("Released: " + btn.getId());
    btn.setOnMouseExited(new EventHandler<MouseEvent>() {

            @Override
        public void handle(MouseEvent event) {
        }
        });
    }
    

    public void nextLevel(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/FinDelJuego.fxml"));
        Parent root = loader.load();
        
        Scene gameOverScene = new Scene(root);
        
        //access the controller and call a method
        FinDelJuegoController gameOverController = loader.getController();
        gameOverController.initData(temp.getText(), primer_puntaje, segundo_puntaje, tercer_puntaje, cuarto_puntaje);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(gameOverScene);
        window.show();
    }      
    
}
    
