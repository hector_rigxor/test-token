/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import Model.anamnesis;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class SeleccionarAlumnoController implements Initializable {

    @FXML 
    private Button btnAtras;
    @FXML
    private Button btnIniciarTestUA;
    
    ArrayList<anamnesis> alumnos = new ArrayList<>();
    
    /**
     * Initializes the controller class.
     */
    /**
     * This method accepts a person to initialize the view
     * @param an_ant
     */
    public void initData(ArrayList<anamnesis> an_ant)
    {
        alumnos = (ArrayList)an_ant;  
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        for (int i = 0; i < alumnos.size(); i++) {
            btnIniciarTestUA.setText(alumnos.get(i).getAl_codigo().getAl_nombre_completo());
        }
        
    }    
    
    @FXML
    public void closeWindows() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/MenuPrincipal.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.btnAtras.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(SeleccionarAlumnoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @FXML
    public void pantallaInicio(ActionEvent event) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/IniciarTest.fxml"));
            Parent root = loader.load();
        
            Scene IniTestScene = new Scene(root);
        
            //SeleccionarAlumnoController controller = loader.getController();
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
            window.setScene(IniTestScene);
            window.show();


        } catch (IOException ex) {
            Logger.getLogger(SeleccionarAlumnoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
