/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class FinDelJuegoController implements Initializable {

    @FXML
    private Button levelUp;
    
    @FXML
    public Label temp;
    
    private int primer_puntaje;
    
    private int segundo_puntaje;
    
    private int tercer_puntaje;
    
    private int cuarto_puntaje;
    
    /**
     * Initializes the controller class.
     */
    /**
     * This method accepts a person to initialize the view
     * @param puntaje
     * @param puntUno
     * @param puntDos
     * @param puntTres
     * @param puntCuatro
     */
    public void initData(String puntaje, int puntUno, int puntDos, int puntTres, int puntCuatro)
    {
        temp.setText(puntaje);
        primer_puntaje = puntUno;
        segundo_puntaje = puntDos;
        tercer_puntaje = puntTres;
        cuarto_puntaje = puntCuatro;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void nextLevel(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/puntajeFinal.fxml"));
        Parent root = loader.load();
        
        Scene endGameScene = new Scene(root);
        
        //access the controller and call a method
        PuntajeFinalController endGameController = loader.getController();
        endGameController.initData(temp.getText(), primer_puntaje, segundo_puntaje, tercer_puntaje, cuarto_puntaje);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(endGameScene);
        window.show();
    }
    
}
