/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class NivelUnoController implements Initializable {

    @FXML
    private Button btn;
    
    @FXML
    private Button levelUp;
    
    public int puntos;
    
    private int instruccion;
    
    @FXML
    public Label temp;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    instruccion = 0;
    puntos = 0;
    levelUp.setVisible(false);
    
    }
        
    
    @FXML
    private void mousePressed(MouseEvent event) {
    btn = (Button) event.getSource();
    //System.out.println("Pressed: " + btn.getId());

    instruccion = instruccion + 1;
    btn.setOnMouseEntered(new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {
        //System.out.println("true");
        }
        });
       
        switch(instruccion) {
  case 1:
    if (btn.getId().equals("cgRojo") && instruccion == 1){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
  case 2:
    if (btn.getId().equals("sgVerde") && instruccion == 2){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 3:
    if (btn.getId().equals("sgRojo") && instruccion == 3){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 4:
    if (btn.getId().equals("cgAmarillo") && instruccion == 4){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 5:
    if (btn.getId().equals("cgAzul") && instruccion == 5){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 6:
    if (btn.getId().equals("cgVerde") && instruccion == 6){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 7:
    if (btn.getId().equals("sgAmarillo") && instruccion == 7){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 8:
    if (btn.getId().equals("cgBlanco") && instruccion == 8){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 9:
    if (btn.getId().equals("sgAzul") && instruccion == 9){ 
                puntos = puntos + 1;
                break;
        }
        else{
                break;
    }
    
    case 10:
    if (btn.getId().equals("sgBlanco") && instruccion == 10){ 
                puntos = puntos + 1;
                levelUp.setVisible(true);
                temp.setText(String.valueOf(puntos));
                break;
        }
        else{
                levelUp.setVisible(true);
                temp.setText(String.valueOf(puntos));
                System.out.println(temp.getText() + " Aqui");
                break;
    }
  default:
    System.out.println("Stage over");
}
        System.out.println("Puntos = "+puntos);
    }    

    @FXML
    private void mouseReleased(MouseEvent event) {
    btn = (Button) event.getSource();
    //System.out.println("Released: " + btn.getId());
    btn.setOnMouseExited(new EventHandler<MouseEvent>() {

            @Override
        public void handle(MouseEvent event) {
        }
        });
    }

public void nextLevel(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/NivelDos.fxml"));
        Parent root = loader.load();
        
        Scene nivelDosScene = new Scene(root);
        
        //access the controller and call a method
        NivelDosController controllerDos = loader.getController();
        controllerDos.initData(temp.getText(), puntos);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(nivelDosScene);
        window.show();
    }        
   
}


