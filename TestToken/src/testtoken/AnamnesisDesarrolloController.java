/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import Model.alumno;
import Model.anamnesis;
import Model.caracteristicasConductuales;
import Model.desarrollo;
import Model.desarrolloMotor;
import Model.evaluadoAnteriormente;
import Model.lenguaje;
import Model.sueño;
import Model.tratadoActualmente;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class AnamnesisDesarrolloController implements Initializable {

    
    @FXML private Button btnAtras;
    @FXML private Button btnSegAna;
    //
    @FXML private ToggleGroup bgEnfImp;
    @FXML private RadioButton rbEnfImpSi;
    private boolean enfImp;
    @FXML private TextField tfEnfImp; 
    //
    @FXML private ToggleGroup bgGolTec;
    @FXML private RadioButton rbGolTecSi;
    private boolean golTec;
    //
    @FXML private ToggleGroup bgSinEpi;
    @FXML private RadioButton rbSinEpiSi;
    private boolean sinEpi;
    //
    @FXML private ToggleGroup bgEnfVis;
    @FXML private RadioButton rbEnfVisSi;
    private boolean enfVis;
    //
    @FXML private ToggleGroup bgEnfOid;
    @FXML private RadioButton rbEnfOidSi;
    private boolean enfOid;
    //
    @FXML private ToggleGroup bgEvoLen1;
    @FXML private RadioButton rbEvoLen1Si;
    private boolean evoLen1;
    //
    @FXML private ToggleGroup bgEvoLen2;
    @FXML private RadioButton rbEvoLen2Si;
    private boolean evoLen2;
    //
    @FXML private ToggleGroup bgEvoLen3;
    @FXML private RadioButton rbEvoLen3Si;
    private boolean evoLen3;
    //
    @FXML private ToggleGroup bgEvoLen4;
    @FXML private RadioButton rbEvoLen4Si;
    private boolean evoLen4;
    //
    @FXML private ToggleGroup bgEvoLen5;
    @FXML private RadioButton rbEvoLen5Si;
    private boolean evoLen5;
    //
    @FXML private TextField tfAdqLen;
    @FXML private TextField tfArtPal;
    @FXML private TextField tfOraSim;
    @FXML private TextField tfHacPid;
    @FXML private TextField tfEscLen;
    @FXML private TextField tfApoyoDe;
    @FXML private TextField tfLugEsp;
    @FXML private TextField tfAprCam;
    //
    @FXML private ToggleGroup bgDesMot1;
    @FXML private RadioButton rbDesMot1Si;
    private boolean desMot1;
    //
    @FXML private ToggleGroup bgDesMot2;
    @FXML private RadioButton rbDesMot2Si;
    private boolean desMot2;
    //
    @FXML private ToggleGroup bgDesMot3;
    @FXML private RadioButton rbDesMot3Si;
    private boolean desMot3;
    //
    @FXML private TextField tfDifDiag;
    @FXML private TextField tfApoyoDe2;
    @FXML private TextField tfLugEsp2;
    //
    @FXML private ToggleGroup bgCarSue1;
    @FXML private RadioButton rbCarSue1Si;
    private boolean carSue1;
    //
    @FXML private ToggleGroup bgCarSue2;
    @FXML private RadioButton rbCarSue2Si;
    private boolean carSue2;
    //
    @FXML private ToggleGroup bgCarSue3;
    @FXML private RadioButton rbCarSue3Si;
    private boolean carSue3;
    //
    @FXML private ToggleGroup bgCarSue4;
    @FXML private RadioButton rbCarSue4Si;
    private boolean carSue4;
    //
    @FXML private TextField tfSueInq;
    @FXML private TextField tfPesFre;
    @FXML private TextField tfDifDor;
    @FXML private TextField tfSonamb;
    //
    @FXML private ToggleGroup bgCarCon1;
    @FXML private RadioButton rbCarCon1Si;
    private boolean carCon1;
    //
    @FXML private ToggleGroup bgCarCon2;
    @FXML private RadioButton rbCarCon2Si;
    private boolean carCon2;
    //
    @FXML private ToggleGroup bgCarCon3;
    @FXML private RadioButton rbCarCon3Si;
    private boolean carCon3;
    //
    @FXML private ToggleGroup bgCarCon4;
    @FXML private RadioButton rbCarCon4Si;
    private boolean carCon4;
    //
    @FXML private TextField tfDemTra;
    @FXML private TextField tfDemInq;
    @FXML private TextField tfPelMuc;
    @FXML private TextField tfResNor;
    //
    @FXML private CheckBox cbNeuA;
    @FXML private CheckBox cbPsiqA;
    @FXML private CheckBox cbPsicA;
    @FXML private CheckBox cbFonoA;
    @FXML private TextField tfOtroEspA;
    @FXML private TextField tfDiagEspA;
    @FXML private TextField tfObsEspA;
    //
    @FXML private CheckBox cbNeuB;
    @FXML private CheckBox cbPsiqB;
    @FXML private CheckBox cbPsicB;
    @FXML private CheckBox cbFonoB;
    @FXML private TextField tfOtroEspB;
    @FXML private TextField tfDiagEspB;
    @FXML private TextField tfTratEspB;
    @FXML private DatePicker tfFechEspB; 
    //
    private anamnesis an_actual;
    private alumno al_actual;
    
    
    
    
    /**
     * Initializes the controller class.
     */
    /**
     * This method accepts a person to initialize the view
     * @param an_ant
     */
    public void initData(anamnesis an_ant)
    {
        an_actual = an_ant;      
        al_actual = an_actual.getAl_codigo();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        enfImp = false; golTec = false; sinEpi = false; enfVis = false; enfOid = false;
        evoLen1 = false; evoLen2 = false; evoLen3 = false; evoLen4 = false; evoLen5 = false;
        desMot1 = false; desMot2 = false; desMot3 = false;
        carSue1 = false; carSue2 = false; carSue3 = false; carSue4 = false;
        carCon1 = false; carCon2 = false; carCon3 = false; carCon4 = false;
    }

    @FXML
    public void closeWindows() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/MenuPrincipal.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.btnAtras.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(AnamnesisDesarrolloController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    public void continuarAnamnesis(ActionEvent event) {

        try {
            
            if (this.bgEnfImp.getSelectedToggle().equals(this.rbEnfImpSi)){
            enfImp = true;
            }
            if (this.bgGolTec.getSelectedToggle().equals(this.rbGolTecSi)){
            golTec = true;
            }
            if (this.bgSinEpi.getSelectedToggle().equals(this.rbSinEpiSi)){
            sinEpi = true;
            }
            if (this.bgEnfVis.getSelectedToggle().equals(this.rbEnfVisSi)){
            enfVis = true;
            }
            if (this.bgEnfOid.getSelectedToggle().equals(this.rbEnfOidSi)){
            enfOid = true;
            }
            if (this.bgEvoLen1.getSelectedToggle().equals(this.rbEvoLen1Si)){
            evoLen1 = true;
            }
            if (this.bgEvoLen2.getSelectedToggle().equals(this.rbEvoLen2Si)){
            evoLen2 = true;
            }
            if (this.bgEvoLen3.getSelectedToggle().equals(this.rbEvoLen3Si)){
            evoLen3 = true;
            }
            if (this.bgEvoLen4.getSelectedToggle().equals(this.rbEvoLen4Si)){
            evoLen4 = true;
            }
            if (this.bgEvoLen5.getSelectedToggle().equals(this.rbEvoLen5Si)){
            evoLen5 = true;
            }
            
            desarrollo des_actual = new desarrollo(enfImp, tfEnfImp.getText(), golTec, sinEpi, enfVis, enfOid);
            lenguaje len_actual = new lenguaje(evoLen1, evoLen2, evoLen3, evoLen4, evoLen5, 
                                  tfApoyoDe.getText(), tfLugEsp.getText());
            
            if (evoLen1 == true) {
                len_actual.setLen_demora_des(tfAdqLen.getText());
            }
            if (evoLen2 == true) {
                len_actual.setLen_articular_des(tfArtPal.getText());
            }
            if (evoLen3 == true) {
                len_actual.setLen_simple_des(tfOraSim.getText());
            }
            if (evoLen4 == true) {
                len_actual.setLen_nohaceloquelepiden_des(tfHacPid.getText());
            }
            if (evoLen5 == true) {
                len_actual.setLen_escuela_cual(tfEscLen.getText());
            }
            
            des_actual.setLenguaje_cod(len_actual);
            
            if (this.bgDesMot1.getSelectedToggle().equals(this.rbDesMot1Si)){
            desMot1 = true;
            }
            if (this.bgDesMot2.getSelectedToggle().equals(this.rbDesMot2Si)){
            desMot2 = true;
            }
            if (this.bgDesMot3.getSelectedToggle().equals(this.rbDesMot3Si)){
            desMot3 = true;
            }
            
            desarrolloMotor desMot_actual = new desarrolloMotor(Integer.valueOf(tfAprCam.getText()), 
                    desMot1, desMot2, desMot3, tfDifDiag.getText(), tfApoyoDe2.getText() , tfLugEsp2.getText());
            
            des_actual.setDesMot_cod(desMot_actual);
            
            if (this.bgCarSue1.getSelectedToggle().equals(this.rbCarSue1Si)){
            carSue1 = true;
            }
            
            if (this.bgCarSue2.getSelectedToggle().equals(this.rbCarSue2Si)){
            carSue2 = true;
            }
            
            if (this.bgCarSue3.getSelectedToggle().equals(this.rbCarSue3Si)){
            carSue3 = true;
            }
            
            if (this.bgCarSue4.getSelectedToggle().equals(this.rbCarSue4Si)){
            carSue4 = true;
            }
            
            sueño sue_actual = new sueño(carSue1, carSue2, carSue3, carSue4);
            
            if (carSue1 == true) {
                sue_actual.setSp_inquieto_des(tfSueInq.getText());
            }
            
            if (carSue2 == true) {
                sue_actual.setSp_pesadillas_des(tfPesFre.getText());
            }
            
            if (carSue3 == true) {
                sue_actual.setSp_dificultad_des(tfDifDor.getText());
            }
            
            if (carSue4 == true) {
                sue_actual.setSp_sonambulo_des(tfSonamb.getText());
            }
            
            des_actual.setSueño_cod(sue_actual);
            
            if (this.bgCarCon1.getSelectedToggle().equals(this.rbCarCon1Si)){
            carCon1 = true;
            }
            
            if (this.bgCarCon2.getSelectedToggle().equals(this.rbCarCon2Si)){
            carCon2 = true;
            }
            
            if (this.bgCarCon3.getSelectedToggle().equals(this.rbCarCon3Si)){
            carCon3 = true;
            }
            
            if (this.bgCarCon4.getSelectedToggle().equals(this.rbCarCon4Si)){
            carCon4 = true;
            }
            
            caracteristicasConductuales cc_actual = new caracteristicasConductuales(carCon1, carCon2, carCon3, carCon4);
            
            if (carCon1 == true) {
                cc_actual.setCc_tranquilo_des(tfDemTra.getText());
            }
            
            if (carCon2 == true) {
                cc_actual.setCc_inquieto_des(tfDemInq.getText());
            }
            
            if (carCon3 == true) {
                cc_actual.setCc_pelea_des(tfPelMuc.getText());
            }
            
            if (carCon4 == true) {
                cc_actual.setCc_no_respeta_normas_des(tfResNor.getText());
            }
            
            des_actual.setCarCon_cod(cc_actual);
            
            evaluadoAnteriormente evaAnt_actual = new evaluadoAnteriormente(cbNeuA.isSelected(), cbPsiqA.isSelected()
                    , cbPsicA.isSelected(), cbFonoA.isSelected(), tfOtroEspA.getText(), tfDiagEspA.getText(), tfObsEspA.getText());
                   
            des_actual.setEvaAnt_cod(evaAnt_actual);
            
            tratadoActualmente traAct_actual = new tratadoActualmente(cbNeuB.isSelected(), cbPsiqB.isSelected(), cbPsicB.isSelected(), 
                    cbFonoB.isSelected(), tfOtroEspB.getText(), tfDiagEspB.getText(), tfTratEspB.getText(), tfFechEspB.getValue());
            
            des_actual.setTraAct_cod(traAct_actual);
            
            al_actual.setDes_cod(des_actual);
            
            an_actual.setAl_codigo(al_actual);
            
                    
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/AnamnesisFinal.fxml"));
        Parent root = loader.load();
        
        Scene AnaFinScene = new Scene(root);
        
        //access the controller and call a method
        AnamnesisFinalController controllerAnaFin = loader.getController();
        controllerAnaFin.initData(an_actual);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(AnaFinScene);
        window.show();

        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }    
    
}
