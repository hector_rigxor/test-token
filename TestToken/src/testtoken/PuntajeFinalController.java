/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class PuntajeFinalController implements Initializable {

    private int puntos;
    
    @FXML
    private Button levelUp;
    
    @FXML
    public Label unoBuenas;
    
    @FXML
    public Label unoMalas;
    
    @FXML
    public Label unoTotal;
    
    @FXML
    public Label dosBuenas;
    
    @FXML
    public Label dosMalas;
    
    @FXML
    public Label dosTotal;
    
    @FXML
    public Label tresBuenas;
    
    @FXML
    public Label tresMalas;
    
    @FXML
    public Label tresTotal;
    
    @FXML
    public Label cuatroBuenas;
    
    @FXML
    public Label cuatroMalas;
    
    @FXML
    public Label cuatroTotal;
    
    @FXML
    public Label totalBuenas;
    
    @FXML
    public Label totalMalas;
    
    @FXML
    public Label totalFinal;
    
    /**
     * Initializes the controller class.
     */
    
    /**
     * This method accepts a person to initialize the view
     * @param puntaje
     * @param puntUno
     * @param puntDos
     * @param puntTres
     * @param puntCuatro
     */
    public void initData(String puntaje, int puntUno, int puntDos, int puntTres, int puntCuatro)
    {
        puntos = Integer.parseInt(puntaje);
        unoBuenas.setText(Integer.toString(puntUno));
        unoMalas.setText(Integer.toString(10 - puntUno));
        unoTotal.setText(Integer.toString(puntUno));
        
        dosBuenas.setText(Integer.toString(puntDos));
        dosMalas.setText(Integer.toString(10 - puntDos));
        dosTotal.setText(Integer.toString(puntDos));
        
        tresBuenas.setText(Integer.toString(puntTres));
        tresMalas.setText(Integer.toString(10 - puntTres));
        tresTotal.setText(Integer.toString(puntTres));
        
        cuatroBuenas.setText(Integer.toString(puntCuatro));
        cuatroMalas.setText(Integer.toString(10 - puntCuatro));
        cuatroTotal.setText(Integer.toString(puntCuatro));
        
        totalBuenas.setText(puntaje);
        totalMalas.setText(Integer.toString(40 - puntos));
        totalFinal.setText(puntaje);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
