/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class NivelDosController implements Initializable {
    
    @FXML
    private Button btn;
    
    @FXML
    private Button levelUp;
    
    private int puntos;
    
    private int instruccion;
    @FXML
    public Label temp;
    
    private int primer_puntaje;
    
    private int segundo_puntaje;
    /**
     * Initializes the controller class.
     */
    
    /**
     * This method accepts a person to initialize the view
     * @param puntaje
     * @param puntUno
     */
    public void initData(String puntaje, int puntUno)
    {
        temp.setText(puntaje);
        primer_puntaje = puntUno;
        System.out.println("Nivel uno: "+primer_puntaje);
        puntos = Integer.valueOf(temp.getText());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    instruccion = 0;
    levelUp.setVisible(false);
    segundo_puntaje = 0;
    
    }

    @FXML
    private void mousePressed(MouseEvent event) {
    btn = (Button) event.getSource();
    //System.out.println("Pressed: " + btn.getId());

    instruccion = instruccion + 1;
    btn.setOnMouseEntered(new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {
        //System.out.println("true");
        }
        });
       
        switch(instruccion) {
  case 1:
    if (btn.getId().equals("cpAmarillo") && instruccion == 1){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
  case 2:
    if (btn.getId().equals("cgVerde") && instruccion == 2){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 3:
    if (btn.getId().equals("cgAmarillo") && instruccion == 3){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 4:
    if (btn.getId().equals("sgAzul") && instruccion == 4){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 5:
    if (btn.getId().equals("cpVerde") && instruccion == 5){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 6:
    if (btn.getId().equals("cgRojo") && instruccion == 6){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 7:
    if (btn.getId().equals("sgBlanco") && instruccion == 7){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 8:
    if (btn.getId().equals("cpAzul") && instruccion == 8){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 9:
    if (btn.getId().equals("spVerde") && instruccion == 9){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                break;
        }
        else{
                break;
    }
    
    case 10:
    if (btn.getId().equals("cgAzul") && instruccion == 10){ 
                puntos = puntos + 1;
                segundo_puntaje = segundo_puntaje + 1;
                levelUp.setVisible(true);
                temp.setText(String.valueOf(puntos));
                break;
        }
        else{
                levelUp.setVisible(true);
                temp.setText(String.valueOf(puntos));
                System.out.println(temp.getText() + " Aqui");
                break;
    }
  default:
    System.out.println("Stage over");
}
        System.out.println("Puntos = "+puntos);
    }


    @FXML
    private void mouseReleased(MouseEvent event) {
    btn = (Button) event.getSource();
    //System.out.println("Released: " + btn.getId());
    btn.setOnMouseExited(new EventHandler<MouseEvent>() {

            @Override
        public void handle(MouseEvent event) {
        }
        });
    }   
    
    public void nextLevel(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/NivelTres.fxml"));
        Parent root = loader.load();
        
        Scene nivelTresScene = new Scene(root);
        
        //access the controller and call a method
        NivelTresController controllerTres = loader.getController();
        controllerTres.initData(temp.getText(), primer_puntaje, segundo_puntaje);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(nivelTresScene);
        window.show();
    }  
    
}
