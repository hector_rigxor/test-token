/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import Model.alumno;
import Model.anamnesis;
import Model.madre;
import Model.padre;
import Model.tutor;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class AnamnesisAlumnoController implements Initializable {

    
    @FXML 
    private Button btnAtras;
    @FXML
    private DatePicker tfFec;
    @FXML
    private TextField tfPsi;
    @FXML
    private TextField tfNomAlu;
    @FXML
    private TextField tfRutAlu;
    @FXML
    private DatePicker tfFecNac;
    @FXML
    private TextField tfEda;
    @FXML
    private TextField tfCur;
    @FXML
    private TextField tfDir;
    @FXML
    private TextField tfNomMam;
    @FXML
    private TextField tfConMam;
    @FXML
    private TextField tfRutMam;
    @FXML
    private TextField tfNomPap;
    @FXML
    private TextField tfConPap;
    @FXML
    private TextField tfRutPap;
    @FXML
    private TextField tfNomTut;
    @FXML
    private TextField tfConTut;
    @FXML
    private TextField tfRutTut;
    @FXML
    private Button btnSigAna;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void closeWindows(ActionEvent event) {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/MenuPrincipal.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
            
            myStage.setScene(scene);
            myStage.show();

        } catch (IOException ex) {
            Logger.getLogger(AnamnesisAlumnoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    public void continuarAnamnesis(ActionEvent event) {
        
        try {
            
            alumno al_actual = new alumno(tfRutAlu.getText(), tfNomAlu.getText(), tfFecNac.getValue(), Integer.valueOf(tfEda.getText()), tfCur.getText(), tfDir.getText());
            anamnesis an_actual = new anamnesis(tfFec.getValue(), tfPsi.getText(), al_actual);
            madre ma_actual = new madre(tfRutMam.getText(), tfNomMam.getText(), tfConMam.getText());
            padre pa_actual = new padre(tfRutPap.getText(), tfNomPap.getText(), tfConPap.getText());
            tutor tu_actual = new tutor(tfRutTut.getText(), tfNomTut.getText(), tfConTut.getText());
            al_actual.setMadre_rut(ma_actual);
            al_actual.setPadre_rut(pa_actual);
            al_actual.setTutor_rut(tu_actual);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/AnamnesisDesarrollo.fxml"));
        Parent root = loader.load();
        
        Scene AnaDesScene = new Scene(root);
        
        //access the controller and call a method
        AnamnesisDesarrolloController controllerAnaDes = loader.getController();
        controllerAnaDes.initData(an_actual);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(AnaDesScene);
        window.show();

        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
