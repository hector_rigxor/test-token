/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import Model.alumno;
import Model.anamnesis;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class MenuPrincipalController implements Initializable {

    AnchorPane di;
    private anamnesis an_actual;
    private alumno al_actual;
    @FXML
    private Button btnDesIns;
    @FXML
    private Button btnReaAna;
    @FXML
    private Button btnReaTest;
    @FXML
    NivelUnoController nivelUno;
    @FXML
    NivelDosController nivelDos;
    ArrayList<anamnesis> anamnesisA = new ArrayList<>();
    
    
    /**
     * Initializes the controller class.
     */
    /**
     * This method accepts a person to initialize the view
     * @param an_ant
     */
    public void initData(anamnesis an_ant)
    {
        an_actual = an_ant;      
        al_actual = an_actual.getAl_codigo();
        anamnesisA.add(an_actual);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
    }    
    
    @FXML
    private void verInstrucciones(ActionEvent event) {

        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/DescargarInstrucciones.fxml"));
            Parent root = loader.load();
        
            Scene DesInsScene = new Scene(root);
        
            //AnamnesisAlumnoController controller = loader.getController();
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
            window.setScene(DesInsScene);
            window.show();

        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @FXML
    private void realizarAnamnesis(ActionEvent event) {

            try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/AnamnesisAlumno.fxml"));
            Parent root = loader.load();
        
            Scene AnaAluScene = new Scene(root);
        
            //AnamnesisAlumnoController controller = loader.getController();
        
            //This line gets the Stage information
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
            window.setScene(AnaAluScene);
            window.show();

        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @FXML
    private void realizarTest(ActionEvent event) {

        try {
            
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/SeleccionarAlumno.fxml"));
        Parent root = loader.load();
        
        Scene SelAluScene = new Scene(root);
        
        //access the controller and call a method
        SeleccionarAlumnoController controllerSeleccion = loader.getController();
        controllerSeleccion.initData(anamnesisA);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(SelAluScene);
        window.show();
        
        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
