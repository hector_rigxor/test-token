/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtoken;

import Model.alumno;
import Model.anamnesis;
import Model.antecedentesEscuela;
import Model.antecedentesFamilia;
import Model.madre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Rigxo
 */
public class AnamnesisFinalController implements Initializable {
    
    @FXML private Button btnAtras;
    private anamnesis an_actual;
    private alumno al_actual;
    //
    @FXML private ToggleGroup bgAsisJar;
    @FXML private RadioButton rbAsisJarSi;
    private boolean asisJar;
    //
    @FXML private ToggleGroup bgRepCur;
    @FXML private RadioButton rbRepCurSi;
    private boolean repCur;
    
    @FXML private TextField tfDifAsig;
    @FXML private TextField tfJardin;
    @FXML private TextField tfKinderPre;
    @FXML private TextField tfRepCurso;
    @FXML private TextField tfRepCursoCau;
    //
    @FXML private ToggleGroup bgEnfFam;
    @FXML private RadioButton rbEnfFamSi;
    private boolean enfFam;
    //
    @FXML private TextField tfHogNi;
    @FXML private TextField tfEnfFamCual;
    @FXML private TextField tfWhoCuida;
    @FXML private TextField tfWhoAyud;
    @FXML private TextField tfInfExt;         
    @FXML private TextField tfWhoRes;
    //
    @FXML private TextField tfOcuMad;
    @FXML private TextField tfEdadMad;         
    @FXML private TextField tfCursoMad;
    //
    @FXML private TextField tfOcuPad;
    @FXML private TextField tfEdadPad;         
    @FXML private TextField tfCursoPad;
    
    /**
     * Initializes the controller class.
     */
    /**
     * This method accepts a person to initialize the view
     * @param an_ant
     */
    public void initData(anamnesis an_ant)
    {
        an_actual = an_ant;      
        al_actual = an_actual.getAl_codigo();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        asisJar = false;
        repCur = false;
        enfFam = false;
    }

    @FXML
    public void closeWindows() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/MenuPrincipal.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.btnAtras.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(AnamnesisDesarrolloController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

@FXML
    public void finalizarAnamnesis(ActionEvent event) {

        try {
            
            if (this.bgAsisJar.getSelectedToggle().equals(this.rbAsisJarSi)){
            asisJar = true;
            }
            if (this.bgRepCur.getSelectedToggle().equals(this.rbRepCurSi)){
            repCur = true;
            }
            
            antecedentesEscuela anEs_actual = new antecedentesEscuela(asisJar, repCur, tfDifAsig.getText());
            
            if (asisJar == true) {
                anEs_actual.setAe_jardin_nombre(tfJardin.getText());
                anEs_actual.setAe_kinder_prekinder(tfKinderPre.getText());
            }
            
            if (repCur == true) {
                anEs_actual.setAe_repetido_curso(tfRepCurso.getText());
                anEs_actual.setAe_repetido_razon(tfRepCursoCau.getText());
            }
            
            al_actual.setAntEsc_cod(anEs_actual);
            
            if (this.bgEnfFam.getSelectedToggle().equals(this.rbEnfFamSi)){
            enfFam = true;
            }
            
            antecedentesFamilia anFam_actual = new antecedentesFamilia(tfHogNi.getText(), enfFam, tfWhoCuida.getText()
                    , tfWhoAyud.getText(), tfInfExt.getText(), tfWhoRes.getText());
                    
             if (enfFam == true) {
                anFam_actual.setAf_enfermedad_familiar_des(tfEnfFamCual.getText());
                
            }       
            
            al_actual.setAntFam_cod(anFam_actual);
            
            al_actual.getMadre_rut().setMadre_ocupacion(tfOcuMad.getText());
            al_actual.getMadre_rut().setMadre_edad(Integer.parseInt(tfEdadMad.getText()));
            al_actual.getMadre_rut().setMadre_escolaridad(tfCursoMad.getText());
            
            al_actual.getPadre_rut().setPadre_ocupacion(tfOcuPad.getText());
            al_actual.getPadre_rut().setPadre_edad(Integer.parseInt(tfEdadPad.getText()));
            al_actual.getPadre_rut().setPadre_escolaridad(tfCursoPad.getText());
            
            an_actual.setAl_codigo(al_actual);
            
                    
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/MenuPrincipal.fxml"));
        Parent root = loader.load();
        
        Scene MenuScene = new Scene(root);
        
        //access the controller and call a method
        MenuPrincipalController controllerMenu = loader.getController();
        controllerMenu.initData(an_actual);
        
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(MenuScene);
        window.show();

        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }    
    
}
